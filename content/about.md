---
title: About me
subtitle: Why you'd want to hang out with me
comments: false
---

I am a full stack developer experienced in Python, C, and Web Development. I
spend most of my time on Linux writing software and contributing to open
source. I'm a quick learner and enjoy learning anything related to computers
and technology.

Here is my [resume](//gitlab.com/seanbehan/resume/raw/master/resume.pdf).

Want to hire me? Email me at [seanwbehan@riseup.net](mailto:seanwbehan@riseup.net)
